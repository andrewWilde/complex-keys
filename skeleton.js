(function(){

  //-------------------------- lib -----------------------------------
  function complexKeys(){
    var lastobj,

        toStringFactory = function(Type){
          var orig = Type.prototype.toString,
              proto = Object.prototype,
              hash = function(){ return parseInt(Math.random() * 100000000, 10); }; //TODO: real hash fun

          return function(){
            var str = orig.call(this);

            this.__hash === undefined &&
            Object.defineProperty(this, '__hash', {
              value: hash(),
              enumerable: false
            });
            
            lastobj = this;

            !proto.hasOwnProperty(str) && defineProperty(str);
            return str;
          };
        },

        defineProperty = function(prop){
          Object.defineProperty(Object.prototype, prop, {
            set: function(val){
              this.__store = this.__store || {};

              lastobj && (this.__store[lastobj.__hash] = { key: lastobj, value: val }) ||
              (this.__store['__value' + prop] = { key: prop, value: val });

              lastobj = undefined;
            },

            get: function(){
              var o = this.__store && lastobj && this.__store[lastobj.__hash],
                  v = o && o.value,
                  k = '__value' + prop;

              !o && (v = this.__store[k] && this.__store[k].value || undefined);
              lastobj = undefined;

              return v;
            }
          });        
        };


    [Object, Array, Function, RegExp].forEach(function(T){
      T.prototype.toString = toStringFactory(T);
    });

    Object.keys = (function(){
      var orig = Object.keys;

      return function(a){
        var r = orig.call(Object, a),
            i = r.indexOf('__store'),
            splice = Array.prototype.splice.bind(r, i, 1);

        i >= 0 && splice.apply(r, Object.keys(a.__store).map(function(k){ return a.__store[k].key }));

        return r;
      };
    })();

  };

  // ----------------------- demo --------------------------------

  complexKeys(); //init

  var test = {};

  //------- object --------------
  var object = { key: 'value' };
  
  test[object] = 'value0';
  console.log(test[object]);

  // ------ array ---------------
  var array = [1, 2, 3];
  
  test[array] = 'value1';
  console.log(test[array]);

  // ------ function ---------------
  var fun = function(){ return true; };
  
  test[fun] = 'value2';
  console.log(test[fun]);

  // ------ regexp ---------------
  var regexp = /123/;
  
  test[regexp] = 'value3';
  console.log(test[regexp]);

  // ------ number ---------------
  var number = 123;
  
  test[number] = 'value4';
  console.log(test[number]);

  // ------ string ---------------
  var string = 'sample';
  
  test[string] = 'value5';
  console.log(test[string]);

  //-------- keys ---------------
  console.log('Object.keys(test) = ', Object.keys(test));
  /*
  console output:
  value0
  value1
  value2
  value3
  value4
  value5
  Object.keys(test) =  [ '123', /123/, [ 1, 2, 3 ], { key: 'value' }, [Function], 'sample' ]
  */
})();